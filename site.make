core = 7.x
api = 2

; cacheexclude
projects[cacheexclude][type] = "module"
projects[cacheexclude][download][type] = "git"
projects[cacheexclude][download][url] = "https://git.uwaterloo.ca/drupal-org/cacheexclude.git"
projects[cacheexclude][download][tag] = "7.x-2.4"
projects[cacheexclude][subdir] = ""

; uw_network_alerts
projects[uw_network_alerts][type] = "module"
projects[uw_network_alerts][download][type] = "git"
projects[uw_network_alerts][download][url] = "https://git.uwaterloo.ca/wcms/uw_network_alerts.git"
projects[uw_network_alerts][download][tag] = "7.x-2.0"
projects[uw_network_alerts][subdir] = ""

; uw_wifi_charts
projects[uw_wifi_charts][type] = "module"
projects[uw_wifi_charts][download][type] = "git"
projects[uw_wifi_charts][download][url] = "https://git.uwaterloo.ca/wcms/uw_wifi_charts.git"
projects[uw_wifi_charts][download][tag] = "7.x-1.1"
projects[uw_wifi_charts][subdir] = ""

; uw_cfg_ist
projects[uw_cfg_ist][type] = "module"
projects[uw_cfg_ist][download][type] = "git"
projects[uw_cfg_ist][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_ist.git"
projects[uw_cfg_ist][download][tag] = "7.x-1.0"
projects[uw_cfg_ist][subdir] = ""

; uw_cfg_wifi_charts
projects[uw_cfg_wifi_charts][type] = "module"
projects[uw_cfg_wifi_charts][download][type] = "git"
projects[uw_cfg_wifi_charts][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_wifi_charts.git"
projects[uw_cfg_wifi_charts][download][tag] = "7.x-1.0"
projects[uw_cfg_wifi_charts][subdir] = ""

